#!/usr/bin/sh
#***************************************************************************
# Nombre      : sh_abif_oed_exp.sh
# Ruta        : /SinacOED/she/
# Autor       : Francisco Valenzuela (Ada Ltda.) - Daniel Araya Ureta (Ing. Software. Bci)
# Fecha       : 20/05/2021
# Descripcion : Shell valida archivo de datos OED y crea archivo de control para luego validar su informaion
# Parametros  : yyyymmdd
#             :	es el dia posterior al domingo del rango (lunes a domingo de extracion)
# Ejemplo de Ejecucion  : sh sh_abif_oed_exp.sh 20210517 
#***************************************************************************
# Mantencion  :
# Empresa     :
# Fecha       :
# Descripcion :
#***************************************************************************

#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	AHORA=`date +'%Y%m%d%H%M'`

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/tmp/${BSE_PGM}_${INF_PID}.log
export archLog


#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
unifica_archivos_log()
{
	printlog 1 "Unificando en el log archivos para su analisis"

	if [ -f ${DirTmp}/${INF_PID}*.tmp ]
	then
		for FILE in ${DirTmp}/${INF_PID}*.tmp
		do
			printlog 2 "-------------------------------------------------------------"
			printlog 2 "Archivo: $FILE"
			printlog 2 "Contenido:"
			printlog 2 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			cat $FILE| grep -i -v "\.logon" >> ${archLog}
			printlog 2 "--------------------- FIN DE ARCHIVO ------------------------"
			eliminar_archivo $FILE
		done
	fi
}

printlogfinal()
{	
	printlog 2 "valida si log existe"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi
	
	printlog 2 "valida si log tiene datos"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### ##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1"
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de Profile..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}


#poner fecha de proceso que ingresa por parametro, correspondiente a la fecha de envio del archivo
fec_prc=$1


Valida_num_param()
{
	# $1 Cantidad de parametros a validar
	# $2 Cantidad de parametros correctas
	
	printlog 2 "Funcion Valida_num_param - Parametro 1: $1"
	printlog 2 "Funcion Valida_num_param - Parametro 2: $2"
	
	if [ $1 -ne $2 ] 
	then
		printlog 2 "Funcion Valida_num_param : ERROR - La cantidad de parametros de entrada NO COINCIDE con el requerido"
		Syntax
		SALIR 2  " FALLIDO - La cantidad de parametros no son los requeridos"
	fi
}



#Funcion valida Fecha Proceso
validafechaentrada() 
{
	#$1 Fecha a validar
	
	# Validacion del largo de la cadena
	lenstr=$(expr length $1)
	if [ ! $lenstr -eq 8 ]
	then
		printlog 1 "Funcion validaPeriodo : ERROR - La cadena de la fecha debe tener 8 caracteres"
		SALIR 2 " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	#Validacion del anno
	anno=$(expr substr $1 1 4)
	if [ $anno -le 1900 ]
	then
 		printlog 1 "Funcion validaPeriodo : ERROR - El anno debe ser mayor o igual a 1900"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	# Validacion del mes
	mes=$(expr substr $1 5 2)
	if [ $mes -lt 1 ]
	then
		printlog 1 "Funcion validaPeriodo : ERROR - El numero del mes debe ser mayor o igual a 1"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	if [ $mes -gt 12 ]
	then 
		printlog 1 "Funcion validaPeriodo : ERROR - El numero del mes debe ser menor o igual a 12"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	#Validacion del dia
	dia=`expr substr $1 7 2`
	if [ $dia -lt 1 ]
	then
		printlog 1 "Funcion validaPeriodo : ERROR - El num. del dia debe ser mayor o igual a 1"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	if [ $dia -gt 31 ]
	then 
		printlog 1 "Funcion validaPeriodo : ERROR - El numero del dia debe ser menor o igual a 31"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	return 1
}

valida_definicion_variables () {
if [ "x$1" = "x" ]
then
	printlog 1 "Funcion valida_definicion_variables - Error, no hay variables a validar"
	SALIR 2  " FALLIDO - No se han definido variables"
fi

for V in $1
do
	if [ "x$V" = "x" ]
	then
		printlog 1 "Funcion valida_definicion_variables - ERROR: La variable de ambiente $V no esta definida"
		SALIR 2 " FALLIDO - No se han definido la variable $V"
	else
		printlog 2 "Funcion valida_definicion_variables - La variable de ambiente $V se encuentra definida"
	fi
done
}

# Elimina el archivo validando previamente si existe
eliminar_archivo () 
{
	if [ -f $1 ]
	then
		rm -f $1
		printlog 2 "Se elimina archivo $1"
	else
		printlog 2 "Archivo $1 no puede ser eliminado. Motivo:"
		if [ -d $1 ]
		then
			printlog 2 "Archivo $1 es un directorio."
		else
			printlog 2 "Archivo $1 no existe"
		fi
	fi
}


# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t sh ${INF_PGM} FECHA_PROCESO"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "sh ${INF_PGM} 20151004"

}

#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de Funciones
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/abif_oed_Profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile

#valida definicion de variables de ambiente
valida_definicion_variables "${VARIABLE_AMBIENTE}"

#valida parametro de entrada
Valida_num_param $# $Cant_Min_Param


##############################################################
################### INICIO EJECUCION SHELL ###################
##############################################################

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${AHORA}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi

#valida fecha de parametro que entra por shell
validafechaentrada $fec_prc


#Funcion valida fecha en registros del archivo de datos
validaPeriodo() 
{
	#$1 Fecha a validar
	
lenstr=$(expr length $1)
anno=$(expr substr $1 1 4)
mes=$(expr substr $1 5 2)
dia=`expr substr $1 7 2`

	
	# Validacion del largo de la cadena

	if [ $lenstr -eq 8 ]
	then
		val_fec=1
		export val_fec
		
		if [ $anno -ge 1900 ]
			#Validacion del anno
			then
				val_fec=1
				export val_fec

					# Validacion del mes
					
					if [ $mes -ge 1 -a $mes -le 12 ]
					then
						val_fec=1
						export val_fec
					
									#Validacion del dia
									
									if [ $dia -ge 1 -a $dia -le 31 ]
									then
										val_fec=1
										export val_fec
									

									
									else
										val_fec=5
										export val_fec

									fi		
					else
						val_fec=6
						export val_fec

					fi
		else
			val_fec=7
			export val_fec
		fi		
			
		
	else
		val_fec=8
		export val_fec
	
	fi
}


# valida un rut valido
valida_rut ()
{
rut=$1
div=1
serie=2
sum=0
verificador=0
partida=0
termino=8
while [ $partida -le $termino ]; do

 temp=$((rut/div))
               div=$(($div*10))
               if [ $serie = "8" ]; then
                       serie=2
               fi
               sum=$(((((($temp-((((rut/div))*10))))*serie))+$sum))
               serie=$(($serie+1))
       let partida=partida+1
done

verificador=$((11-(($sum%11))))
if [ $verificador = "10" ]; then
    verificador=k
fi
if [ $verificador = "11" ];then
    verificador=0
fi
export verificador
}


#valada existencia de archivo de entrada
func_valida_archivo $Nom_ach_entrada



#cantidad de registros del archivo de datos antes de ser validado
NumReg=`wc -l $Nom_ach_entrada | awk '{ printf "%i", $1 }'`

#nombre de archivo con datos correctos
nom_arch_correc=${DirTmp}/ArchivoCorrecto_OED_${AHORA}.dat

#nombre de archivo con datos erroneos
nom_arch_error=${DirFtpOut}/ArchivoError_OED_${fec_prc}.dat


#elimina archivo temporal con datos correctos por reproceso
eliminar_archivo $nom_arch_correc

#elimina archivo con datos erroneos por reproceso
eliminar_archivo $nom_arch_error


#Inicia un ciclo para obtener detalle registro por registro
printlog 1 "Inicia un ciclo para obtener detalle registro por registro"
inicio=1
while [ $inicio -le $NumReg ]; do


#variables utilizadas para guardar cada campo del archivo de datos, los cuales entran a la validacion
rut_cli=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $1}'`

dv_cli1=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $2}'`
#convierte a minuscula
dv_cli=`expr $dv_cli1 | tr '[:upper:]' '[:lower:]'`

mov_cod_tra=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $3}'`
#cuenta caracteres en numero de operacion
cant_caracteres=$(expr length $mov_cod_tra)

med_pag=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $4}'`
fec_ope=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $5}'`
fec_rec=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $6}'`
cnl_rec=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $7}'`
tip_cmp=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $8}'`
tip_acc=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $9}'`
rec_mot=`sed -n ${inicio}p $Nom_ach_entrada | awk 'BEGIN {FS=";"}; {print $10}'`

#variable con la linea del registro completo del archivo de datos, que es utilizado para guardar
#en archivo bueno o malo segun validacion
linea_dato="$rut_cli;$dv_cli;$mov_cod_tra;$med_pag;$fec_ope;$fec_rec;$cnl_rec;$tip_cmp;$tip_acc;$rec_mot"


#VALIDACION REGISTRO EN ARCHIVO DE DATOS
#INICIO
  if [[ $rut_cli -lt 50000000 ]]                                                                      
  	then    
	    valida_rut $rut_cli
		if [ $dv_cli = $verificador ]
			then
				if [ $cant_caracteres -le 20 ]
					then
						if [ $med_pag = '1' -o $med_pag = '2' -o $med_pag = '3' -o $med_pag = '4' ]                                                                      
							then 
								validaPeriodo $fec_ope
								if [ $val_fec = 1 ]
									then
										validaPeriodo $fec_rec
										if [ $val_fec = 1 ]
											then
												if [ $cnl_rec = '1' -o $cnl_rec = '2' ]                                                                      
													then 
											
														if [ $tip_cmp = '1' -o $tip_cmp = '2' ]                                                                      
															then                                                                            
												
																if [ $tip_acc = 'I' -o $tip_acc = 'E' ]                                                                      
																	then                                                                            
												
																		if [ $rec_mot = 'D' -o $rec_mot = 'E' -o $rec_mot = 'C' ]                                                                      
																			then       
																			
																				if [ $tip_acc = 'I' -a $rec_mot = 'C' ]                                         
																					then
																						echo $linea_dato >> $nom_arch_correc
																					elif [ $tip_acc = 'E' -a $rec_mot = 'E' ]
																					then
																						echo $linea_dato >> $nom_arch_correc
																					
																					elif	[ $tip_acc = 'E' -a $rec_mot = 'D' ]
																					then
																						echo $linea_dato >> $nom_arch_correc	
																				else
																					echo $linea_dato >> $nom_arch_error																			
																				fi 
																		else
																			echo $linea_dato >> $nom_arch_error
																		fi 
																		
																else
																	echo $linea_dato >> $nom_arch_error
																fi  
																	
														else
															echo $linea_dato >> $nom_arch_error
														fi  
														
												else	
													echo $linea_dato >> $nom_arch_error
												fi 
												
										else	
											echo $linea_dato >> $nom_arch_error
										fi  
													
								else	
									echo $linea_dato >> $nom_arch_error
								fi
								
						else
							echo $linea_dato >> $nom_arch_error
						fi 
				else
					echo $linea_dato >> $nom_arch_error
				fi 
			
		else
			echo $linea_dato >> $nom_arch_error
		fi 
				
 else
	echo $linea_dato >> $nom_arch_error		
 fi    
                                                                         
#FIN

let inicio=inicio+1
done

#valida existencia de archivo con datos correctos despues de validacion
func_valida_archivo $nom_arch_correc

#######################################################################
#### inicio construccion de registro de control en archivo de datos ####
#######################################################################


fec_arch=$fec_prc
NumReg02=`wc -l $nom_arch_correc | awk '{ printf "%i", $1 }'`

Nombre_archivo_final_sdir=$cod_inst$datos$fec_arch$nun_corr_dia$tip_car$ext_arc_dat
Nombre_archivo_final=${DirFtpOut}/$cod_inst$datos$fec_arch$nun_corr_dia$tip_car$ext_arc_dat

#elimina archivo final con datos correctos por reproceso
eliminar_archivo $Nombre_archivo_final

echo "$rec_ctr;$cod_inst;$fec_arch;$NumReg02">>$Nombre_archivo_final #se debe agregar al principio del archivo

cat $nom_arch_correc>>$Nombre_archivo_final 


#elimina archivo de paso
eliminar_archivo $nom_arch_correc

#valida existencia de archivo con datos final
func_valida_archivo $Nombre_archivo_final
printlog 2 "nombre del archivo de datos $Nombre_archivo_final"

#VALIDACION REGISTRO DE CONTROL EN ARCHIVO DE DATOS
#guarda en variable registro de control
reg_control=`sed -n 1p $Nombre_archivo_final | awk 'BEGIN {FS=";"}; {print $1}'`
#guarda en variable codigo institucional
cod_inst=`sed -n 1p $Nombre_archivo_final | awk 'BEGIN {FS=";"}; {print $2}'`
#guarda en variable fecha del archivo
fec_archivo=`sed -n 1p $Nombre_archivo_final | awk 'BEGIN {FS=";"}; {print $3}'`
#guarda en variable numero de registros
num_reg=`sed -n 1p $Nombre_archivo_final | awk 'BEGIN {FS=";"}; {print $4}'`
#guarda en variable numero de caracteres de la variable num_reg, no debe superar los 6
cant_car_reg=$(expr length $num_reg)

printlog 2 " variable registro de control: $reg_control "
printlog 2 " variable codigo institucional: $cod_inst "
printlog 2 " variable fecha del archivo: $fec_archivo "
printlog 2 " variable numero de registros: $num_reg "
printlog 2 " variable numero de caracteres de la variable num_reg, no debe superar los 6 caracteres: $cant_car_reg "

  if [ $reg_control = $rec_ctr ]                                                                      
  	then 
		if [ $cod_inst = $cod_inst ]
			then 
				validaPeriodo $fec_archivo
						if [ $val_fec = 1 ]
							then
								if [ $cant_car_reg -le 6 -a $num_reg -eq $NumReg02 ]
									then 
										printlog 1 "CORRECTO,  registro de control en archivo de datos"
										printlog 2 "CORRECTO,  registro de control en archivo de datos"
								else
									printlog 1 "Error en registro de control en archivo de datos, cantidad de registros"
									printlog 2 "Error en registro de control en archivo de datos, cantidad de registros"
								fi
						else
							printlog 1 "Error en registro de control en archivo de datos, fecha de archivo"
							printlog 2 "Error en registro de control en archivo de datos, fecha de archivo"
						fi
		else
			printlog 1 "Error en registro de control en archivo de datos, codigo institucional"
			printlog 2 "Error en registro de control en archivo de datos, codigo institucional"
		fi
else
	printlog 1 "Error en registro de control en archivo de datos, registro de control"
	printlog 2 "Error en registro de control en archivo de datos, registro de control"
fi		



###########################################################
###### variables para componer el archivo de control ######
###########################################################

#nombre del archivo de datos sin extencion
Nom_arch_fin_sinext=$cod_inst$datos$fec_arch$nun_corr_dia$tip_car

#nombre del archivo de control
Nom_arch_ctrl=${DirFtpOut}/$cod_inst$datos$fec_prc$nun_corr_dia$tip_car$ext_arc_ctr
printlog 2 "nombre del archivo de control $Nom_arch_ctrl"

#elimina archivo de control por reproceso
eliminar_archivo $Nom_arch_ctrl

#se utiliza como variable de nombre para comprimir el archivo
Nom_arch_ctrl_sdir=$cod_inst$datos$fec_prc$nun_corr_dia$tip_car$ext_arc_ctr

#variables para el calculo del peso dela rchivo
#1.- tamano del archivo
tam_reg_arch=`wc -c $Nombre_archivo_final | awk '{printf "%i", $1}'`
#2.- cantidad de registros incluido el EOF, ultima linea vacia
cant_reg_arch=`wc -l $Nombre_archivo_final | awk '{printf "%i", $1}'`
#3.-al peso o tamano del archivo, se le resta la cantidad de registros con EOF
valor_arch=`expr $tam_reg_arch - $cant_reg_arch`


#agrega ceros a la izquierda
valor_arch0=`printf "%010d" $valor_arch`
cant_reg_arch0=`printf "%010d" $cant_reg_arch`


printlog 2 " tamano del archivo de datos: $tam_reg_arch "
printlog 2 " cantidad de registros incluido el EOF, ultima linea vacia: $cant_reg_arch "
printlog 2 " resta entre tamano del archivo menos cantidad de registro: $valor_arch "


#contenido del archivo de control
echo $ini_msg_ctr>$Nom_arch_ctrl
printf "%-20s" $Nom_arch_fin_sinext $originador_ctr $destino_ctr $cant_reg_arch0$lf_lv_ctr$valor_arch0$fort_ori$traduc >> $Nom_arch_ctrl
echo '\n'$fin_msg_ctr>>$Nom_arch_ctrl



#Comprimir archivo de respaldo
#archivo de datos
cp $Nombre_archivo_final ${DirBkp}/$Nombre_archivo_final_sdir'_'${AHORA}
compress -f ${DirBkp}/$Nombre_archivo_final_sdir'_'${AHORA}

#archivo de control
cp $Nom_arch_ctrl ${DirBkp}/$Nom_arch_ctrl_sdir'_'${AHORA}
compress -f ${DirBkp}/$Nom_arch_ctrl_sdir'_'${AHORA}

eliminar_archivo $Nom_ach_entrada

#elimina historia de archivo de respaldo
#se eliminan todos los archivo que tengan antiguedad mas de un anno
find ${DirBkp}/$cod_inst$datos*.Z -type f -mtime +365  -exec rm {} \;

# Finaliza la ejecucion
printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"

