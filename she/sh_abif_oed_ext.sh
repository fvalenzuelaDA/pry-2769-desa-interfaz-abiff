#!/usr/bin/sh
#***************************************************************************
# Nombre      : sh_abif_oed_ext.sh
# Ruta        : /SinacOED/she/
# Autor       : Francisco Valenzuela (Ada Ltda.) - Daniel Araya Ureta (Ing. Software. Bci)
# Fecha       : 02/03/2021
# Descripcion : Extrae operaciones electronicas desconocidas con un rango de una semana (Luneas a Domingo) desde bd Postgress
# Parametros  : yyyymmdd
#             :	es el dia posterior al domingo del rango (lunes a domingo de extracion)
# Ejemplo de Ejecucion  : sh sh_abif_oed_ext.sh 20210517 
#***************************************************************************
# Mantencion  :
# Empresa     :
# Fecha       :
# Descripcion :
#***************************************************************************


#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_STT=0
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	AHORA=`date +'%Y%m%d%H%M'`

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/tmp/${BSE_PGM}_${INF_PID}.log
export archLog


#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
unifica_archivos_log()
{
	printlog 1 "Unificando en el log archivos para su analisis"

	if [ -f ${DirTmp}/${INF_PID}*.tmp ]
	then
		for FILE in ${DirTmp}/${INF_PID}*.tmp
		do
			printlog 2 "-------------------------------------------------------------"
			printlog 2 "Archivo: $FILE"
			printlog 2 "Contenido:"
			printlog 2 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			cat $FILE| grep -i -v "\.logon" >> ${archLog}
			printlog 2 "--------------------- FIN DE ARCHIVO ------------------------"
			eliminar_archivo $FILE
		done
	fi
}

printlogfinal()
{	
	printlog 2 "valida si log existe"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi
	
	printlog 2 "valida si log tiene datos"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### ##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1"
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de Profile..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}



#poner fecha de proceso que ingresa por parametro, correspondiente a la fecha posterior al domingo de la semana de extraccion
fec_prc=$1

Valida_num_param()
{
	# $1 Cantidad de parametros a validar
	# $2 Cantidad de parametros correctas
	
	printlog 2 "Funcion Valida_num_param - Parametro 1: $1"
	printlog 2 "Funcion Valida_num_param - Parametro 2: $2"
	
	if [ $1 -ne $2 ] 
	then
		printlog 2 "Funcion Valida_num_param : ERROR - La cantidad de parametros de entrada NO COINCIDE con el requerido"
		Syntax
		SALIR 2  " FALLIDO - La cantidad de parametros no son los requeridos"
	fi
}


#Funcion valida Fecha Proceso
validafechaentrada() 
{
	#$1 Fecha a validar
	
	# Validacion del largo de la cadena
	lenstr=$(expr length $1)
	if [ ! $lenstr -eq 8 ]
	then
		printlog 1 "Funcion validaPeriodo : ERROR - La cadena de la fecha debe tener 8 caracteres"
		SALIR 2 " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	#Validacion del anno
	anno=$(expr substr $1 1 4)
	if [ $anno -le 1900 ]
	then
 		printlog 1 "Funcion validaPeriodo : ERROR - El anno debe ser mayor o igual a 1900"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	# Validacion del mes
	mes=$(expr substr $1 5 2)
	if [ $mes -lt 1 ]
	then
		printlog 1 "Funcion validaPeriodo : ERROR - El numero del mes debe ser mayor o igual a 1"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	if [ $mes -gt 12 ]
	then 
		printlog 1 "Funcion validaPeriodo : ERROR - El numero del mes debe ser menor o igual a 12"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	#Validacion del dia
	dia=`expr substr $1 7 2`
	if [ $dia -lt 1 ]
	then
		printlog 1 "Funcion validaPeriodo : ERROR - El num. del dia debe ser mayor o igual a 1"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	if [ $dia -gt 31 ]
	then 
		printlog 1 "Funcion validaPeriodo : ERROR - El numero del dia debe ser menor o igual a 31"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	return 1
}


valida_definicion_variables () {
if [ "x$1" = "x" ]
then
	printlog 1 "Funcion valida_definicion_variables - Error, no hay variables a validar"
	SALIR 2  " FALLIDO - No se han definido variables"
fi

for V in $1
do
	if [ "x$V" = "x" ]
	then
		printlog 1 "Funcion valida_definicion_variables - ERROR: La variable de ambiente $V no esta definida"
		SALIR 2 " FALLIDO - No se han definido la variable $V"
	else
		printlog 2 "Funcion valida_definicion_variables - La variable de ambiente $V se encuentra definida"
	fi
done
}

# Elimina el archivo validando previamente si existe
eliminar_archivo () 
{
	if [ -f $1 ]
	then
		rm -f $1
		printlog 2 "Se elimina archivo $1"
	else
		printlog 2 "Archivo $1 no puede ser eliminado. Motivo:"
		if [ -d $1 ]
		then
			printlog 2 "Archivo $1 es un directorio."
		else
			printlog 2 "Archivo $1 no existe"
		fi
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t sh ${INF_PGM} FECHA_PROCESO"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "sh ${INF_PGM} 20151004"

}



#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de consults sql
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/abif_oed_Profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile

# Verifica archivo de consulta sql
func_valida_archivo $consulta

#valida definicion de variables de ambiente
valida_definicion_variables "${VARIABLE_AMBIENTE}"

#valida parametro de entrada
Valida_num_param $# $Cant_Min_Param


##############################################################
################### INICIO EJECUCION SHELL ###################
##############################################################

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${AHORA}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi

#valida fecha de parametro que entra por shell
validafechaentrada $fec_prc


# Funcion uypuser conexion a bd postgres
setuservar()
{
   UYPRSP=`UyPuser $1`
   if [[ $? != 0 ]] then
      printlog 2 "ERROR : Se cancelo la ejecucion de UyPuser"
      printlog 2 "ERROR : SISTEMA = $1 "
      printlog 2 "ERROR : RETORNO = ${UYPRSP}"
      P_STAT="ERR"
   else
      uypcnt=0
      for uyprsp in ${UYPRSP}
      do
        uypDa[$uypcnt]=$uyprsp
        (( uypcnt=$uypcnt+1))
      done
            if [ "${uypDa[0]}" = "ERROR" ] ; then
               printlog 2 "ERROR : No se pudo obtener la clave del sistema $1"
               printlog 2 "ERROR : RETORNO = ${uypDa[1]}"
               printlog 2 "ERROR : se aborta ejecucion"
            else
               P_USUARIO=${uypDa[0]}
               P_CLAVE=${uypDa[1]}
               P_STAT="OK"
            fi
   fi
   printlog 2 "- Obtencion de clave para sistema ( $1 ) ${P_STAT}"
}


# Funcion conexion a bd postgres
Consulta_Postgress ()
{
PGPASSWORD=${P_CLAVE} psql -h $1 -p $2 -U $P_USUARIO$useramb -d $3 -v $5 -f $4 > $Nom_ach_entrada

#--------------------------------------------------------------------------------
#Siempre validar que el comando se ejecuto en forma correcta
#--------------------------------------------------------------------------------
if [[ $? != 0 ]] then
	printlog 2 "ERROR : La ejecucion de consulta"
	P_STAT="ERR"
else
	P_STAT="OK"
fi
printlog 2 "- Ejecuccion de consulta y generacion de archivo de salida $P_STAT"

}


#---------------------------------------------------------------------
#  Definicion de valiables locales
#---------------------------------------------------------------------

P_USUARIO=""
P_CLAVE=""
P_STAT=OK

printlog 2 "*********************************************************"
printlog 2 "** EJECUCION: ${INF_PGM}  PID=${INF_PID}   ${INF_INI} "
printlog 2 "*********************************************************"



fechasql2=$(expr substr $fec_prc 1 4)'-'$(expr substr $fec_prc 5 2)'-'$(expr substr $fec_prc 7 2)
fechasql="'$fechasql2'"

printlog 2 "** fecha ingresada a la bd: ${fechasql} "

# Obtencion de usuario y password
#----------------------------------
setuservar "${sist}"

if [[ "${P_STAT}" = "ERR" ]] ; then
   SALIR 2 "Obtencion de usuario y password incorrecto"
fi

Consulta_Postgress $server $puerto $bd $consulta v1=$fechasql

if [[ "${P_STAT}" = "ERR" ]] ; then
   SALIR 2 "Conexion a Postgress Incorrecta"
fi

printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"

