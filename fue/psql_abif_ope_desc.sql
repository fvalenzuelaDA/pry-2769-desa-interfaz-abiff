------------------------------------------------------------------------------
-- Nombre      : psql_abif_ope_desc.sql
-- Ruta        : /SinacOED/fue/
-- Autor       : Francisco Valenzuela (Ada Ltda.) - Daniel Araya Ureta (Ing. Software. Bci)
-- Fecha       : 20/05/2021
-- Descripcion : proceso sql que extrae informacion de OED desde postgres
-- Parametros  : :v1 --> 'yyyy-mm-dd'
--             : fecha con la cual se obtiene el ultimo domingo y el lunes antes de dia mensionado,
--			: con esto se puede tener un rango de 7 dias de informacion ( de lunes a domingo)
------------------------------------------------------------------------------
-- Mantencion  :
-- Empresa     :
-- Fecha       :
-- Descripcion :
------------------------------------------------------------------------------

COPY (
--calcula el mu,ero del dia correspondiente a la fecha de entrada
-- donde 0 es domingo y 6 es lunes
WITH num_dia AS (select cast(date_part('dow',date :v1) as integer) as num)

--extrae la informacion de OED
select 
e.eve_rut_cliente --rut del cliente
,e.eve_dv_cliente --digito verificador de cliente
,('OED'||(10000*extract(year from e.eve_fec_hms_ingreso)+100*extract(month from e.eve_fec_hms_ingreso)+extract(day from e.eve_fec_hms_ingreso))||(to_char(row_number()OVER(),'fm000000000'))) as numerooperacion -- numero de operacion inventado
,case when mov_gls_transaccion in ('Transferencias') then 1
	when mov_gls_transaccion in ('Giros','Pago Con Tarjeta De Debito') then 3
	when mov_gls_transaccion in ('Pagos en Linea') then 4
	else null end medio_pago  --medio de pago
,10000*extract(year from m2.mov_fec_hms_transaccion)+100*extract(month from m2.mov_fec_hms_transaccion)+extract(day from m2.mov_fec_hms_transaccion) FechaOperacion -- fecha de operacion
,10000*extract(year from e.eve_fec_hms_ingreso)+100*extract(month from e.eve_fec_hms_ingreso)+extract(day from e.eve_fec_hms_ingreso)  FechaReclamacion -- fecha de reclamo
,case when mov_gls_transaccion in ('Transferencias') then 2
	when mov_gls_transaccion in ('Giros','Pago Con Tarjeta De Debito') then 1
	when mov_gls_transaccion in ('Pagos en Linea') then 2
	else null end  canal --canal
,1 TipoCompra 	--tipo de compra
,'I' TipoAccion --tipo de accion
,'C' motivo	--motivo
from evento e
join movimiento m2
on e.eve_id = m2.eve_id
join estado_evento ee
on   ee.ese_id = e.ese_id
join  motivo m3
on m3.mot_id = e.mot_id
where mov_gls_transaccion in ('Pagos en Linea','Giros','Pago Con Tarjeta De Debito','Transferencias')
and (cast(eve_fec_hms_ingreso as date))<=(select date :v1 - num from num_dia)
and (cast(eve_fec_hms_ingreso as date))>=(select ((date :v1 - num) - 6) from num_dia)
and e.eve_rut_cliente < 50000000
)TO STDOUT (format CSV, delimiter ';')
